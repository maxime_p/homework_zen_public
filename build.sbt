name := """homework_zen"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  filters,
  "org.scalaz" %% "scalaz-core" % "7.2.0",
  evolutions,
  "com.typesafe.play" %% "anorm" % "2.5.0",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.14",
  "mysql" % "mysql-connector-java" % "5.1.6"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
