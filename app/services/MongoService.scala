package services

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import reactivemongo.api.ReadPreference
import play.modules.reactivemongo.{ MongoController, ReactiveMongoApi, ReactiveMongoComponents }
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import models.PokemonModel._

/**
  * Service that use MongoDB to act like a cache
  */
class MongoService @Inject() (
                                val reactiveMongoApi: ReactiveMongoApi
                               ) extends MongoController with ReactiveMongoComponents {

  /** Associate a type name with a sequence of pokemon's names
    *
    * @param name type name
    * @param list pokemon's names
    */
  case class ListPokemon(name: String, list: Seq[String])

  // JS reader/writer
  implicit val listPokemon = Json.format[ListPokemon]

  private def pokemons = database.map(_.collection[JSONCollection]("pokemons"))
  private def listPokemons = database.map(_.collection[JSONCollection]("listsPokemons"))

  /** Add a pokemon Object from the API */
  def addPokemon(pokemon: Pokemon) = {

    pokemons.flatMap(_.insert(pokemon))
  }

  /** Get a pokemon Object as if it was coming from the API */
  def getPokemon(name: String): Future[Option[Pokemon]] = {

    pokemons.flatMap {
      _.find(Json.obj("name" -> name))
        .cursor[Pokemon](ReadPreference.primary)
        .collect[Seq]()
        .map(_.headOption)
    }
  }

  /** Add a sequence of all the pokemon's name */
  def addAllPokemons(seqStrings: Seq[String]) = {

    listPokemons.flatMap(_.insert(ListPokemon("all", seqStrings)))
  }

  /** Get a sorted sequence of pokemon's name containing a query */
  def getAllPokemons(query: String): Future[Option[Seq[String]]] = {

    listPokemons.flatMap{
      _.find(Json.obj("name" -> "all"))
        .cursor[ListPokemon](ReadPreference.primary)
        .collect[Seq]()
        .map{
          _.headOption.map{
            _.list.filter{ i =>
              i.contains(query)
            }.map{ x =>
              PokeName(x, x.indexOfSlice(query))
            }.sortBy(r => (r.index, r.name)).map(_.name)
          }
        }
    }
  }

  /** Add a sequence of pokemon's name according to a type */
  def addPokemonsOfType(typeName: String, seqStrings: Seq[String]) = {

    listPokemons.flatMap(_.insert(ListPokemon(typeName, seqStrings)))
  }

  /** Get a sorted sequence of pokemon's name according to a type */
  def getPokemonsOfType(typeName: String): Future[Option[Seq[String]]] = {

    listPokemons.flatMap{
      _.find(Json.obj("name" -> typeName))
      .cursor[ListPokemon](ReadPreference.primary)
      .collect[Seq]()
      .map{
        _.headOption.map{
          _.list
        }
      }
    }
  }
}