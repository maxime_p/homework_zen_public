package services

import javax.inject.Inject

import models.{VoteModel, VoteRepository}
import play.api.db.Database

/**
  * Service that links VoteRepository to the DB
  */
class VoteService @Inject()(
                             DB: Database,
                             VR:  VoteRepository
                           ){

  /** count the number of like or dislike occurrences for a pokemon */
  def countByPokemonAndValue(pokemon: String, value: Int): Long = {
    DB.withConnection { implicit connection =>
      VR.countByPokemonAndValue(pokemon, value: Int)
    }
  }

  /** get the vote to a pokemon from an IP */
  def findByPokemonAndIP(pokemon: String, ip: String): Option[VoteModel] = {
    DB.withConnection { implicit connection =>
      VR.findByPokemonAndIP(pokemon, ip)
    }
  }

  /** find a vote by ID */
  def findById(id: Long): Option[VoteModel] = {
    DB.withConnection { implicit connection =>
      VR.findById(id)
    }
  }

  /** find a vote by uuid */
  def findByUuid(uuid: String): Option[VoteModel] = {
    DB.withConnection { implicit connection =>
      VR.findByUuid(uuid)
    }
  }

  /** find every votes */
  def findAll: Seq[VoteModel] = {
    DB.withConnection { implicit connection =>
      VR.findAll
    }
  }

  /** update a vote */
  def update(model: VoteModel): Unit = {
    DB.withConnection { implicit connection =>
      VR.update(model)
    }
  }

  /** create a vote */
  def create(model: VoteModel): Option[VoteModel] = {
    DB.withConnection { implicit connection =>
      VR.create(model)
    }
  }

  /** delete a vote */
  def delete(id: Long): Unit = {
    DB.withConnection { implicit connection =>
      VR.delete(id)
    }
  }
}
