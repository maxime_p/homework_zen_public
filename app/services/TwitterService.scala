package services

import javax.inject.Inject
import play.api.Configuration
import play.api.libs.ws.WSClient
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaz.syntax.std.option._
import java.util.Base64
import java.nio.charset.StandardCharsets
import play.api.libs.json.JsValue

/**
  * Service that calls Twitter API
  */
class TwitterService @Inject()(WS: WSClient,
                              conf: Configuration
                             ) {

  // Oauth2 config
  lazy val key = conf.underlying.getString("twitter.consumer.key")
  lazy val secret = conf.underlying.getString("twitter.consumer.secret")
  lazy val url = conf.underlying.getString("twitter.url.auth")

  /** Get all latest tweets of a given hashtag */
  def getHashTag(name: String) : Future[Option[JsValue]] = {

    val token = Base64.getEncoder.encodeToString(s"${key}:${secret}".getBytes(StandardCharsets.UTF_8))

    // get the access token by post to twitter
    WS.url(url)
      .withHeaders(
        ("Authorization" -> s"Basic $token"),
        ("Content-Type" -> "application/x-www-form-urlencoded;charset=UTF-8")
      )
      .post("grant_type=client_credentials")
      .flatMap{ response =>
        val access_token = (response.json \ "access_token").asOpt[String]
        access_token.map { token =>

          // then request for data
          WS.url(s"https://api.twitter.com/1.1/search/tweets.json?q=%23$name")
            .withHeaders("Authorization" -> s"Bearer $token")
            .get
            .map{ tweets =>
              Some(tweets.json)
            }.recover{
            case e:java.net.ConnectException => None
          }
        } | Future(None)
      }.recover{
      case e:java.net.ConnectException => None
    }
  }
}