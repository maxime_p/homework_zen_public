package services

import play.api.libs.json._
import play.api.libs.ws._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import javax.inject.Inject
import play.api.Logger
import models.PokemonModel.{PokeName, PokeNameType, Pokemon, PokemonSearchList}
import models._

/**
  * Service that request Pokeapi.co for data
  */
class PokemonService @Inject() (
                                 WS: WSClient,
                                 MS: MongoService
                               ) {

  /** Get data of a pokemon by its name,
    * uses the API only if not in MongoDB
    * otherwise adds the pokemon in it
    */
  def getPokemon(name: String): Future[Option[Pokemon]] = {

    MS.getPokemon(name).flatMap{ optionList =>
      optionList match {
        case Some(l) => Future {
          Logger.debug(s"Utilisation du cache 'getPokemon($name)'")
          optionList
        }
        case None => {
          Logger.debug(s"Utilisation de l'API 'getPokemon($name)'")
          WS.url(s"http://pokeapi.co/api/v2/pokemon/$name")
            .withHeaders("Content-Type" -> "application/json")
            .get
            .map { response =>
              try{
                val pokemonResponse = response.json.validate[Pokemon](PokemonModel.pokemon)
                pokemonResponse match {
                  case s: JsSuccess[Pokemon] => {
                    val pokeOpt = s.asOpt
                    pokeOpt match {
                      case Some(p) => {
                        MS.addPokemon(p)
                        pokeOpt
                      }
                      case None => None
                    }
                  }
                  case e: JsError =>
                    Logger.error("Parsing failed !")
                    None
                }
              } catch {
                case e: Exception =>
                  Logger.error(s"Bad JSON or timed out ! 'getPokemon($name)'")
                  None
              }
            }.recover{
            case e:java.net.ConnectException =>
              Logger.error("Connection failed !")
              None
          }
        }
      }
    }
  }

  /** Get all the pokemon's names,
    * uses the API only if not in MongoDB
    * otherwise adds the list in it
    */
  def getAllPokemons(query: String): Future[Option[Seq[String]]] = {

    MS.getAllPokemons(query).flatMap{ optionList =>
      optionList match {
        case Some(l) => Future{
          Logger.debug(s"Utilisation du cache 'getAllPokemons($query)'")
          optionList
        }
        case None => {
          Logger.debug(s"Utilisation de l'API 'getAllPokemons($query)'")
          WS.url("http://pokeapi.co/api/v2/pokemon/?limit=811")
            .withHeaders("Content-Type" -> "application/json")
            .get
            .map { response =>
              try{
                val pokemonsResponse = response.json.validate[PokemonSearchList](PokemonModel.pokemonSearchList)
                (pokemonsResponse match {
                  case s: JsSuccess[PokemonSearchList] => s.asOpt
                  case e: JsError =>
                    Logger.error("Parsing failed !")
                    None
                }).map{ result =>
                  val names = result.results.map(_.name)
                  MS.addAllPokemons(names)
                  names.filter{ n =>
                    n.contains(query)
                  }.map{ x =>
                    PokeName(x, x.indexOfSlice(query))
                  }.sortBy(r => (r.index, r.name)).map(_.name)
                }
              } catch {
                case e: Exception =>
                  Logger.error("Bad JSON or timed out !")
                  None
              }
            }.recover{
            case e:java.net.ConnectException =>
              Logger.error("Connection failed !")
              None
          }
        }
      }
    }
  }

  /** Get all the pokemon's names from a type,
    * uses the API only if not in MongoDB
    * otherwise adds the list in it
    */
  def getPokemonsOfType(typeName: String): Future[Option[Seq[String]]] = {

    MS.getPokemonsOfType(typeName).flatMap{ optionList =>
      optionList match {
        case Some(l) => Future {
          Logger.debug(s"Utilisation du cache 'getPokemonsOfType($typeName)'")
          optionList
        }
        case None => {
          Logger.debug(s"Utilisation de l'API 'getPokemonsOfType($typeName)'")
          WS.url(s"http://pokeapi.co/api/v2/type/$typeName")
            .withHeaders("Content-Type" -> "application/json")
            .get
            .map { response =>
              try{
                val pokemonsResponse = (response.json \ "pokemon").get.validate[Seq[PokeNameType]]
                (pokemonsResponse match {
                  case s: JsSuccess[Seq[PokeNameType]] => s.asOpt
                  case e: JsError =>
                    Logger.error("Parsing failed !")
                    None
                }).map{ result =>
                  val names = result.map(_.pokemon.name)
                  MS.addPokemonsOfType(typeName, names)
                  names
                }
              } catch {
                case e: Exception =>
                  Logger.error("Bad JSON or timed out !"+e)
                  None
              }
            }.recover{
            case e:java.net.ConnectException =>
              Logger.error("Connection failed !")
              None
          }
        }
      }
    }
  }
}
