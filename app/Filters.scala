import javax.inject._
import play.api.http.DefaultHttpFilters
import play.filters.gzip.GzipFilter

/**
 * This class configures filters that run on every request. This
 * class is queried by Play to get a list of filters.
 */
@Singleton
class Filters @Inject() (gzipFilter: GzipFilter) extends DefaultHttpFilters(gzipFilter){

}
