package controllers

import javax.inject._
import services.{PokemonService, VoteService}
import scala.concurrent.ExecutionContext.Implicits.global
import javax.inject.Inject
import models.VoteModel._
import scalaz.syntax.std.option._
import play.api.mvc._

/**
  * This controller handles html pages of the application
  */
@Singleton
class MvcController @Inject()(
                               PS: PokemonService,
                               VS: VoteService
                             ) extends Controller {

  /** Renders the home page */
  def index() = Action { implicit request =>

    val error = request.flash.get("error") | ""
    Ok(views.html.index("My Pokedex", error))
  }

  /** Redirect to the home page if the detail parameter is empty */
  def detailEmpty = Action { implicit request =>

    Redirect(routes.MvcController.index).flashing("error" -> "Please type something.")
  }

  /** Render the detail page */
  def detail(name: String) = Action.async { implicit request =>

    PS.getPokemon(name).map{ pokeOption =>
      pokeOption.map{ p =>

        val origin = request.remoteAddress
        val countL = VS.countByPokemonAndValue(p.name, UP).toInt
        val countD = VS.countByPokemonAndValue(p.name, DOWN).toInt

        VS.findByPokemonAndIP(name, origin).map{ vote =>
          if(vote.value == UP){
            Ok(views.html.detail(s"Detail of ${p.name.capitalize}", p, countL, true, countD, false))
          } else {
            Ok(views.html.detail(s"Detail of ${p.name.capitalize}", p, countL, false, countD, true))
          }
        } | Ok(views.html.detail(s"Detail of ${p.name.capitalize}", p, countL, false, countD, false))
      } | Redirect(routes.MvcController.index).flashing("error" ->"Pokemon not found !")
    }
  }

}