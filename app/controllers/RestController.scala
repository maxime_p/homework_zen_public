package controllers

import javax.inject._

import models.VoteModel
import models.VoteModel._
import play.api.libs.json.{JsBoolean, JsValue, Json}
import services.{PokemonService, TwitterService, VoteService}
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaz.syntax.std.option._
import scalaz.std.option._
import scalaz.syntax.monad._

/**
  * This controller handles every REST call from Ajax
  */
@Singleton
class RestController @Inject()(
                                TS: TwitterService,
                                PS: PokemonService,
                                VS: VoteService
                              ) extends Controller {

  /** A dot on the radar graph
    *
    * @param axis label
    * @param value position
    */
  case class Capacity(axis: String, value: Int)

  /** Link a full set of dots with a legend
    *
    * @param name legend
    * @param capacities positions
    */
  case class Chart(name: String, capacities: Seq[Capacity])

  /** A sequence of charts, convenient for JS parsing
    *
    * @param charts sequence of charts
    */
  case class Charts(charts: Seq[Chart])

  // JS readers/writers
  implicit val capacity = Json.format[Capacity]
  implicit val chart = Json.format[Chart]
  implicit val charts = Json.format[Charts]

  /** Add or remove a page to the bookmark session */
  def bookmark = Action(parse.json) { request =>

    // get the url of the page the request is from
    val origin = request.headers.get("Referer")
    val pokemon = (request.body \ "name").asOpt[String]

    (pokemon |@| origin) { (p, o) =>

      request.session.get(p) match {
        case Some(k) => { // return false if not bookmarked anymore
          Ok(JsBoolean(false)).withSession(
            request.session - p
          )
        }
        case None => { // return true if added to bookmarks
          Ok(JsBoolean(true)).withSession(
            request.session + (p -> o)
          )
        }
      }
    } | BadRequest
  }

  /** Return a sorted list of pokemon's names that contain the query */
  def search(query: Option[String]) = Action.async {

    // if query is empty, return all
    PS.getAllPokemons(query.map(_.toLowerCase) | "").map{ listOption =>
      listOption.map{l =>
        Ok(Json.toJson(l))
      } | NotFound(Json.toJson(Seq[JsValue]()))
    }
  }

  /** Return a list of tweets from the Twitter API */
  def getHashTag(name: String) = Action.async {

    TS.getHashTag(name).map{ hashOption =>
      hashOption.map(Ok(_)) | NotFound
    }
  }

  /** Return statistics of a pokemon and its type */
  def getStats(name:String) = Action.async {

    /** Get a Pokemon Object from the API, get its type(s),
      * by type: get the list of all the pokemons of the same type,
      * goes through every pokemon to compute the average scores,
      * then format to serve the graph library
      */
    PS.getPokemon(name).flatMap{ pokeOption =>
      pokeOption.map{p =>

        val capacities = p.stats.map(x =>
          Capacity(x.stat.name, x.base_stat)
        ).sortWith(_.axis < _.axis)  //hack so the graph get not confuse
        val chartPokemon = Chart(p.name.capitalize, capacities)

        Future.sequence(
          p.types.map{ typeObj =>

            val typeName = typeObj.`type`.name
            PS.getPokemonsOfType(typeName).map{ listOption =>
              listOption.map{ pokeNameList =>
                Future.sequence(
                  pokeNameList.map{ pokeName =>

                    PS.getPokemon(pokeName).map { pokeOption =>
                      pokeOption.map { p =>
                        p.stats.map(x => Capacity(x.stat.name, x.base_stat))
                      } | Seq()
                    }
                  }
                ).map{c =>
                  val grouped = c.flatten.groupBy(_.axis).map{ case (k, v) =>
                    Capacity(k, v.map(_.value).sum/v.length) // div by 0 not possible here
                  }.toSeq.sortWith(_.axis < _.axis) //hack so the graph get not confuse with labels order
                  Chart(s"Average of type '${typeName.capitalize}'", grouped)
                }
              }
            }//end all pokemons
          }//end all types
        ).flatMap{ seq =>
          Future.sequence(seq.filter(_.isDefined).map(_.get))
        }.map{ chartTypes =>
          Ok(Json.toJson(Charts(chartTypes:+chartPokemon)))
        }
      } | Future{NotFound}
    }
  }

  /** Add a like to a pokemon */
  def like = Action(parse.json) { request =>

    val origin = request.remoteAddress
    val pokemon = (request.body \ "name").asOpt[String]

    pokemon.map{ p =>

      VS.findByPokemonAndIP(p, origin) match {
        case Some(k) => { // return false if ip not allowed to vote for that pokemon anymore
          if(k.value == DOWN){
            VS.update(k.copy(value = UP))
            Ok(JsBoolean(true))
          } else
          Ok(JsBoolean(false))
        }
        case None => { // return true if allowed
          VS.create(VoteModel(ip = origin, pokemon = p, value = UP))
          Ok(JsBoolean(true))
        }
      }
    } | BadRequest
  }

  /** Add a dislike to a pokemon */
  def dislike = Action(parse.json) { request =>

    val origin = request.remoteAddress
    val pokemon = (request.body \ "name").asOpt[String]

    pokemon.map{ p =>

      VS.findByPokemonAndIP(p, origin) match {
        case Some(k) => { // return false if ip not allowed to vote for that pokemon anymore
          if(k.value == UP){
            VS.update(k.copy(value = DOWN))
            Ok(JsBoolean(true))
          } else
          Ok(JsBoolean(false))
        }
        case None => { // return true if allowed
          VS.create(VoteModel(ip = origin, pokemon = p, value = DOWN))
          Ok(JsBoolean(true))
        }
      }
    } | BadRequest
  }
}
