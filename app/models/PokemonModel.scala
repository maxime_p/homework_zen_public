package models

import play.api.libs.json.Json

/**
  * This object provide classes and JSON parsers to get a Pokemon
  * Object prom the API
  */
object PokemonModel {

  case class Item(url:String,name:String)
  case class Ability(slot:Int,is_hidden:Boolean,ability: Item)
  case class Stat(stat:Item,effort:Int,base_stat: Int)
  case class VersionGroupDetail(move_learn_method:Item,level_learned_at: Int, version_group: Item)
  case class Move(version_group_details:Seq[VersionGroupDetail],move: Item)
  case class Sprite(back_female:Option[String],back_shiny_female:Option[String],back_default:Option[String],front_female:Option[String],
                    front_shiny_female:Option[String],back_shiny:Option[String],front_default:Option[String],front_shiny:Option[String])
  case class VersionDetail(version:Item,rarity:Int)
  case class HeldItem(item:Item,version_details: Seq[VersionDetail])
  case class GameIndice(version:Item,game_index:Int)
  case class Type(`type`:Item,slot:Int)
  case class Pokemon(forms:Seq[Item], abilities:Seq[Ability], stats:Seq[Stat], name: String, weight: Int,
                     moves:Seq[Move], sprites: Sprite, held_items:Seq[HeldItem], location_area_encounters: String,
                     height: Int, is_default: Boolean, species: Item, id: Int, order: Int, game_indices:Seq[GameIndice],
                     base_experience: Int, types:Seq[Type])
  case class PokemonSearchList(count: Int, previous: Option[String], results: Seq[Item], next: Option[String])
  case class PokeName(name: String, index: Int)
  case class PokeNameType(pokemon:Item,slot:Int)


  implicit val item = Json.format[Item]
  implicit val ability = Json.format[Ability]
  implicit val stat = Json.format[Stat]
  implicit val versionGroupDetail = Json.format[VersionGroupDetail]
  implicit val move = Json.format[Move]
  implicit val sprite = Json.format[Sprite]
  implicit val versionDetail = Json.format[VersionDetail]
  implicit val heldItem = Json.format[HeldItem]
  implicit val gameIndice = Json.format[GameIndice]
  implicit val `type` = Json.format[Type]
  implicit val pokemon = Json.format[Pokemon]
  implicit val pokemonSearchList = Json.format[PokemonSearchList]
  implicit val pokeName = Json.format[PokeName]
  implicit val pokeNameType = Json.format[PokeNameType]

}