package models

import java.sql.Connection
import java.util.{Date, UUID}

import anorm.{Macro, RowParser, SqlParser}

import scala.language.postfixOps
import play.api.libs.json._
import anorm._

/** A vote (like or dislike) to a pokemon and from an IP address.
  *
  * @param id internal ID
  * @param uuid external ID
  * @param ip the user's ip (to avoid handling user sessions)
  * @param pokemon the pokmon's name
  * @param value it is an upvote or downvote
  * @param created date of the vote (for later stats on votes)
  */
case class VoteModel(
                       id: Long = 0L,
                       uuid: String = "",
                       ip: String,
                       pokemon: String,
                       value: Int,
                       created: Date = new Date()
                    )

/** Provides SQL and JSON parsers */
object VoteModel {

  val UP = 1
  val DOWN = -1

  implicit val trackFormat: Format[VoteModel] = Json.format[VoteModel]
  val parser = Macro.namedParser[VoteModel].asInstanceOf[RowParser[VoteModel]]
}

/** SQL requests, including basic CRUD */
class VoteRepository {

  /** count the number of like or dislike occurrences for a pokemon */
  def countByPokemonAndValue(pokemon: String, value: Int)(implicit c: Connection): Long = {
    SQL("select count(*) from Vote where pokemon={pokemon} and value={value}").on('pokemon -> pokemon, 'value -> value)
      .as(SqlParser.scalar[Long].single)
  }

  /** get the vote to a pokemon from an IP */
  def findByPokemonAndIP(pokemon: String, ip: String)(implicit c: Connection): Option[VoteModel] = {
    SQL("select * from Vote where ip={ip} and pokemon={pokemon}").on('ip -> ip, 'pokemon -> pokemon)
      .as(VoteModel.parser singleOpt)
  }

  /** find a vote by ID */
  def findById(id: Long)(implicit c: Connection): Option[VoteModel] = {
    SQL("select * from Vote where id={id}").on('id -> id).as(VoteModel.parser singleOpt)
  }

  /** find a vote by uuid */
  def findByUuid(uuid: String)(implicit c: Connection): Option[VoteModel] = {
    SQL("select * from Vote where uuid={uuid}").on('uuid -> uuid).as(VoteModel.parser singleOpt)
  }

  /** find every votes */
  def findAll()(implicit c: Connection): Seq[VoteModel] = {
    SQL("select * from Vote").as(VoteModel.parser *)
  }

  /** update a vote */
  def update(model: VoteModel)(implicit c: Connection): Unit = {
    SQL("update Vote set ip={ip}, pokemon={pokemon}, value={value} where id={id}")
      .on(
        'id -> model.id,
        'ip -> model.ip,
        'pokemon -> model.pokemon,
        'value -> model.value
      ).executeUpdate
  }

  /** create a vote */
  def create(model: VoteModel)(implicit c: Connection): Option[VoteModel] = {
    val uuid = UUID.randomUUID().toString()
    SQL("insert into Vote(uuid, ip, pokemon, value, created) values ({uuid}, {ip}, {pokemon}, {value}, {created})")
      .on(
        'uuid -> uuid,
        'ip -> model.ip,
        'pokemon -> model.pokemon,
        'value -> model.value,
        'created -> new Date()
      ).executeInsert(SqlParser.scalar[Long].singleOpt) match {
      case None => None
      case Some(pk) => Some(model.copy(id = pk, uuid = uuid))
    }
  }

  /** delete a vote */
  def delete(id: Long)(implicit c: Connection): Unit = {
    SQL("delete from Vote where id={id}").on("id" -> id).executeUpdate
  }
}