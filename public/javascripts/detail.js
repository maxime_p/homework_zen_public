var Detail = Detail || {};
(function(exports) {
    function load(pokemon) {

        var firstTwitterLoad = true;

        $('#twitter-refresh').hide();

        $('.pictures-inline').click(function() {

            var source = $(this).find( "a > img" )[0];
            var title = source.alt.replace("_", " ");
            title = title.substr(0,1).toUpperCase()+title.substr(1); //hacky way to get that done...;

            $('#modal-title').html(title);
            $('#modal-content').html('<img alt="'+source.alt+'" src="'+source.src+'">');
            $('#modal-picture').modal('show');

        });

        $('#bookmark').click(function() {

            $('#bookmark').addClass("disabled");

            $.ajax({
                url: '/api/bookmark',
                type: 'PUT',
                data: JSON.stringify({"name": pokemon}),
                contentType: "text/json",
                success: function(data) {
                    if(data){
                        $('#bookmark').html('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> UnBookmark');
                    } else {
                        $('#bookmark').html('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Bookmark');
                    }
                    $('#bookmark').removeClass("disabled");
                },
                error: function(data) {

                }
            })
        });

        $('#button-likes').click(function() {

            if(!$('#button-likes').hasClass("disabled")){
                $('#button-likes').addClass("disabled");
                $.ajax({
                    url: '/api/like',
                    type: 'PUT',
                    data: JSON.stringify({"name": pokemon}),
                    contentType: "text/json",
                    success: function(data) {
                        if(data){
                            $.each($('#button-likes').children(),
                                function(index, item){
                                    if($(item).hasClass('badge')){
                                        var nb = $(item).html();
                                        $(item).html(parseInt(nb)+1);
                                    }
                                });
                        }
                        if($('#button-dislikes').hasClass("disabled")){
                            $('#button-dislikes').removeClass("disabled");
                            $.each($('#button-dislikes').children(),
                                function(index, item){
                                    if($(item).hasClass('badge')){
                                        var nb = $(item).html();
                                        $(item).html(parseInt(nb)-1);
                                    }
                                });
                        }
                    },
                    error: function(data) {

                    }
                })
            }
        });

        $('#button-dislikes').click(function() {

            if(!$('#button-dislikes').hasClass("disabled")){
                $('#button-dislikes').addClass("disabled");
                $.ajax({
                    url: '/api/dislike',
                    type: 'PUT',
                    data: JSON.stringify({"name": pokemon}),
                    contentType: "text/json",
                    success: function(data) {
                        if(data){
                            $.each($('#button-dislikes').children(),
                                function(index, item){
                                    if($(item).hasClass('badge')){
                                        var nb = $(item).html();
                                        $(item).html(parseInt(nb)+1);
                                    }
                                });
                        }
                        if($('#button-likes').hasClass("disabled")){
                            $('#button-likes').removeClass("disabled");
                            $.each($('#button-likes').children(),
                                function(index, item){
                                    if($(item).hasClass('badge')){
                                        var nb = $(item).html();
                                        $(item).html(parseInt(nb)-1);
                                    }
                                });
                        }
                        },
                    error: function(data) {

                    }
                })
            }
        });

        (function poll() {
            function getTwitterFeed() {
                $('#twitter-refresh').show();
                $.ajax({ url: "/api/twitter?name="+pokemon,
                    success: function(data) {

                        $('#tweetfeed').html("");
                        if(data.statuses.length == 0){
                            $('#tweetfeed').append('<li class="list-group-item tweet">No recent enough Tweets :(</li>');
                        }
                        $.each(data.statuses, function(index, item){

                            var content = '<li class="list-group-item tweet">'
                                + '<div class="row">'
                                +'<div class="col-lg-2">'
                                +'<a href="https://twitter.com/'+item.user.screen_name+'" class="thumbnail">'
                                +'<img src="'+item.user.profile_image_url_https+'" alt="">'
                                +'</a>'
                                +'</div>'
                                +'<div class="col-lg-10">'
                                +'<b>'+item.user.name+'</b>'+" @"+item.user.screen_name
                                +'<br/>'
                                +highlightAttags(highlightHashtags(linkify(item.text)))
                                +'<br/>';

                            if(typeof item.entities.media !== 'undefined'){
                                content += '<a href="'+item.entities.media[0].media_url_https+'" class="thumbnail">'
                                    +'<img src="'+item.entities.media[0].media_url_https+'" alt="">'
                                    +'</a>';
                            }
                            content += '<br/><p class="tweet-time">'+relative_time(item.created_at)+'</p>'
                                +'</div>'
                                +'</div>'
                                +'</li>';
                            $('#tweetfeed').append(content);
                        });
                    },
                    error: function(data) {

                        $('#tweetfeed').html("");
                        $('#tweetfeed').append('<li class="list-group-item tweet">We are currently not able to connect with twitter !</li>');
                    }, dataType: "json", complete: poll })
                    .always(function() {

                        $('#twitter-refresh').hide();
                    });
            }
            if(firstTwitterLoad){
                getTwitterFeed();
                firstTwitterLoad = false;
            } else{
                setTimeout(function() {
                    getTwitterFeed();
                }, 10000);
            }
        })();

        // Convert Twitter API Timestamp to "Time Ago"
        function relative_time(time_value) {
            var values = time_value.split(" ");
            time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
            var parsed_date = Date.parse(time_value);
            var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
            var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
            delta = delta + (relative_to.getTimezoneOffset() * 60);

            var r = '';
            if (delta < 60) {
                r = 'a minute ago';
            } else if(delta < 120) {
                r = 'couple of minutes ago';
            } else if(delta < (45*60)) {
                r = (parseInt(delta / 60)).toString() + ' minutes ago';
            } else if(delta < (90*60)) {
                r = 'an hour ago';
            } else if(delta < (24*60*60)) {
                r = '' + (parseInt(delta / 3600)).toString() + ' hours ago';
            } else if(delta < (48*60*60)) {
                r = '1 day ago';
            } else {
                r = (parseInt(delta / 86400)).toString() + ' days ago';
            }

            return r;
        }

        // Create Usable Links
        function linkify(text) {
            return text.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/g, function(m) {
                return m.link(m);
            });
        }

        function highlightHashtags(text) {
            return text.replace(/#[A-Za-z0-9-_é]+/g, function(m) {
                return m.link('https://twitter.com/hashtag/'+m.replace('#', '')+'?src=hash');
            });
        }

        function highlightAttags(text) {
            return text.replace(/@[A-Za-z0-9-_]+/g, function(m) {
                return m.link('https://twitter.com/'+m.replace('@', ''));
            });
        }


        ////////// Radar \\\\\\\\\\

        function getDataRadar() {

            $.ajax({ url: "/api/stats?name="+pokemon,
                success: function(data) {
                    $('#loading').remove();
                    //Legend titles
                    var legends = [];
                    //Data
                    var charts = [];
                    $.each(data.charts, function(index, item){

                        legends.push(item.name);
                        charts.push(item.capacities);
                    });
                    drawRadar(legends, charts);
                },
                error: function(data) {

                }});
        }

        getDataRadar();

        function drawRadar(legends, data) {

            //from http://bl.ocks.org/nbremer/6506614
            var parentContainerW = $('#chart').parent().width();
            var w = parentContainerW*0.6,
                h = parentContainerW*0.6;
            var colorscale = d3.scale.category10();
            //Legend titles
            var LegendOptions = legends;
            //Data
            var d = data;
            //Options for the Radar chart, other than default
            var mycfg = {
                w: w,
                h: h,
                maxValue: 260,
                levels: 13,
                ExtraWidthX: 300,
                TranslateX: 110,
                TranslateY: 50
            }
            //Call function to draw the Radar chart
            //Will expect that data is in %'s
            RadarChart.draw("#chart", d, mycfg);
            ////////////////////////////////////////////
            /////////// Initiate legend ////////////////
            ////////////////////////////////////////////
            var svg = d3.select('#chart')
                .selectAll('svg')
                .append('svg')
                .attr("width", w+300)
                .attr("height", h);
            //Create the title for the legend
            /*var text = svg.append("text")
                .attr("class", "title")
                .attr('transform', 'translate(90,50)')
                .attr("x", w - 10)
                .attr("y", 10)
                .attr("font-size", "12px")
                .attr("fill", "#404040")
                .text("Tire");*/
            //Initiate Legend
            var legend = svg.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 200)
                .attr('transform', 'translate(120,20)');
            //Create colour squares
            legend.selectAll('rect')
                .data(LegendOptions)
                .enter()
                .append("rect")
                .attr("x", w - 65)
                .attr("y", function(d, i){ return i * 20;})
                .attr("width", 10)
                .attr("height", 10)
                .style("fill", function(d, i){ return colorscale(i);});
            //Create text next to squares
            legend.selectAll('text')
                .data(LegendOptions)
                .enter()
                .append("text")
                .attr("x", w - 52)
                .attr("y", function(d, i){ return i * 20 + 9;})
                .attr("font-size", "11px")
                .attr("fill", "#737373")
                .text(function(d) { return d; });
        }

    }
    exports.load = load;
})(Detail);