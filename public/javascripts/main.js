if (window.console) {
  console.log("Welcome to your Pokedex application !");
}

$(document).ready(function() {

    var docHeight = $(window).height();
    var footerHeight = $('#footer').height();

    if($('#search-results').length){

        var footerMargin = docHeight - footerHeight - $('#search-results').position().top - 30;
        $('#search-results').css('max-height', footerMargin + 'px');
    }

});