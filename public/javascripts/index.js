var Index = Index || {};
(function(exports) {
    function load(pokemon) {

        function getDataSuggest() {

            $.ajax({ url: "/api/search?query="+$('#search-bar').val(),
                success: function(data) {

                    $('#search-results').html("");
                    $.each(data, function(index, item){
                        var content = '<a href="/detail/'+item+'" class="list-group-item">'
                            + item.substr(0,1).toUpperCase()+item.substr(1); //hacky way to get that done...
                        +'</a>';

                        $('#search-results').append(content);
                    });
                },
                error: function(data) {

                }});
        }

        getDataSuggest();

        $('#search-bar').on('change paste keyup', function() {
            getDataSuggest();
        });

        $('#search-bar').bind('keyup', function(e) {

            if ( e.keyCode === 13 ) { // 13 is enter key
                var search_input = $('#search-bar').val();
                window.location.href = "/detail/"+search_input;
            }
        });

        $('#search-button').click(function() {
    
            var search_input = $('#search-bar').val();
            window.location.href = "/detail/"+search_input;
        });

    }
    exports.load = load;
})(Index);