# Users schema

# --- !Ups

CREATE TABLE Vote (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    uuid char(36) NOT NULL,
    ip varchar(255) NOT NULL,
    pokemon varchar(255) NOT NULL,
    value int not null,
    created timestamp NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(uuid)
);

# --- !Downs

DROP TABLE Vote;